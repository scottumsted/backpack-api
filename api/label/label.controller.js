'use strict';
var sf = require('../../sf/helpers');
var request = require('request');

exports.getLabel = function(req, res){
  // 1. get return from salesforce
  // 2. get a label based on return details
  // 3. patch salesforce return with tracking number
  // 4. return return to client, including url and tracking number
    var options = {
        'url': "https://na16.salesforce.com/services/data/v26.0/query/?q=select+destination_company__c,+rma__c,+package_sku__c,+item__c,+item_description__c,+retailer__c,+order_id__c+from+Return__c+where+retailer__c='"+req.params.retailer+"'+and+rma__c='"+req.params.rma+"'",
        'headers': {
            'Authorization': ''
        }
    };
    sf.get(req.app, options, function(err, sfRes, body){
        var labelUrl = 'https://wwwtest.fedex.com/OnlineLabel/login.do?labelUserCdDesc=EljayClark&labelPasswordDesc=kJXQTb0qu9';
        var trackingNumber = Math.floor(100000000000 + Math.random() * 900000000000).toString();
        var rma = JSON.parse(body);
        if(rma.totalSize > 0){
            var options = {
                    'url': 'https://na16.salesforce.com'+rma.records[0].attributes.url,
                    'headers': {
                    'Authorization': '',
                    "Content-Type": "application/json"
                },
                'json':true,
                'body':{'return_tracking__c':trackingNumber}
              };
            sf.patch(req.app, options, function(err, sfRes, body){
                var rmaSet = {
                   'item':rma.records[0].Item__c,
                   'item_description':rma.records[0].Item_Description__c,
                   'package_sku':rma.records[0].Package_SKU__c,
                   'rma':rma.records[0].RMA__c,
                   'destination_company':rma.records[0].Destination_Company__c,
                   'tracking_number':trackingNumber,
                   'label_url':labelUrl,
                   'order_id':rma.records[0].Order_Id__c
                };
                var options = {
                    'url': 'http://retailers.ezpz.gs/order/'+rma.records[0].Retailer__c,
                    'headers': {
                        "Content-Type": "application/json"
                    },
                    'json':true,
                    'body' :rmaSet
                };
                request.put(options, function(err, rRes, body){
                    res.json([rmaSet]);
                });
            });
        }else{
            res.json([]);
        }
    });  
};
