'use strict';

var express = require('express');
var controller = require('./label.controller');

var router = express.Router();

router.get('/:retailer/:rma/', controller.getLabel);

module.exports = router;