'use strict';
var sf = require('../../sf/helpers');
var request = require('request');

exports.getOrder = function(req, res){
  // 1. get from retailer
  // 2. push to salesforce
  // 3. return to client
  var options = {
  'url': 'http://retailers.ezpz.gs/order/'+req.params.retailer+'/'+req.params.orderId
  };
  request(options, function(err, rRes, body){
    if(!err && rRes.statusCode == 200){
      console.log(body);
      var order = JSON.parse(body);
      var sfReturn = {
          "Destination_Address__c": order[0].company_address,
          "Destination_City__c": order[0].company_city,
          "Destination_Company__c": order[0].company_name,
          "Destination_Postal__c": order[0].company_postal,
          "Destination_State__c": order[0].company_state,
          "Item__c": order[0].item,
          "Item_Description__c": order[0].item_description,
          "Item_Height__c": order[0].item_height,
          "Item_Length__c": order[0].item_length,
          "Item_Width__c": order[0].item_width,
          "Order_Id__c": order[0].order_id,
          "Origin_Address__c": order[0].customer_address,
          "Origin_City__c": order[0].customer_city,
          "Origin_First__c": order[0].customer_first,
          "Origin_Last__c": order[0].customer_last,
          "Origin_Postal__c": order[0].customer_postal,
          "Origin_State__c": order[0].customer_state,
          "Return_Tracking__c":"",
          "RMA__c": order[0].rma,
          "Retailer__c": order[0].retailer,
          "Package_SKU__c": order[0].package_sku,
          "Name": order[0].retailer+" "+order[0].order_id,
          "Reason__c": req.params.reason
          };
      var options = {
            'url': 'https://na16.salesforce.com/services/data/v26.0/sobjects/Return__c',
            'headers': {
              'Authorization': '',
              "Content-Type": "application/json"
          },
         'json':true,
        'body' :sfReturn
      };
      sf.post(req.app,options, function(err, sfRes, body ){
        res.json(order);  
      });
    }else{
      res.json([]);
    }
  });
  
};