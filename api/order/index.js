'use strict';

var express = require('express');
var controller = require('./order.controller');

var router = express.Router();

router.get('/:retailer/:orderId/:reason', controller.getOrder);

module.exports = router;