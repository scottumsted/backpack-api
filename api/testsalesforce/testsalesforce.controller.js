'use strict';
console.log('loading testsalesforce.controller.js begin');
var sf = require('../../sf/helpers');

// Get list of testsalesforces
exports.index = function (req, res) {
    var options = {
        'url': 'https://na16.salesforce.com/services/data/v26.0/sobjects/',
        'headers': {
            'Authorization': ''
        }
    };
    sf.get(req.app, options, function(err, sfRes, body){
        console.log('callback called');
        res.json(JSON.parse(body));
    });
};
console.log('loading testsalesforce.controller.js end');