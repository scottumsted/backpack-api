console.log('loading sf/helpers.js begin');
var request = require('request');
var _this = this;

module.exports.authorize = function(app){
	var credentials = {
		'url': 'https://na16.salesforce.com/services/oauth2/token',
		'method':'POST',
		'form':{
			'grant_type':'password',
			'client_id':'3MVG9fMtCkV6eLhdRfNiMN97fnUoJ1s5Ph1dtNnWAHQY5Y.vtwGne3fX7KetS6WgZq16ujb7.7C9bkAO5ov91',
			'client_secret':'9084249036812638485',
			'username':'umstedapi@gmail.com',
			'password':'MakeMoney1MpN3umsgCIAqcKWAWxuDjVrF',
			'request_uri':'https://localhost'
		}
	};
	request(credentials, 
		function(err,res,body){
			if(!err && res.statusCode == 200){
				var sfAccess = JSON.parse(body);
				app.set('sfAccess', sfAccess);
				console.log('body:'+body);
			}else{
				app.set('sfAccess', {});
				console.log('sf.authorize error:'+body);
			}
		});	
};

module.exports.testAccessToken = function(app, res){
	var options = {
		'url': 'https://na16.salesforce.com/services/data/v26.0/sobjects/',
		'headers':{
			'Authorization': ''
		}
	};
	var sfAccess = app.get('sfAccess');
	options.headers.Authorization = 'Bearer '+sfAccess.access_token;
	request(options, function(err, sfRes, body){
		if(!err && sfRes.statusCode == 200){
			console.log('sf good');
		} else {
			console.log('sf bad');
		}
		res.json(JSON.parse(body));
	});
};

module.exports.post = function(app, options, callback, reauth){
    reauth = typeof reauth !== 'undefined' ? reauth : true;
	var sfAccess = app.get('sfAccess');
	if(options.headers === undefined){
        options.headers = {'Authorization':'Bearer '+sfAccess.access_token};
    }else{
        options.headers.Authorization = 'Bearer '+sfAccess.access_token;
    }
	request.post(options, function(err, res, body){
		if(!err && res.statusCode >= 200 && res.statusCode < 300){
			callback(err, res, body);
		} else if(!err && body[0] !== undefined && body[0].errorCode == 'INVALID_SESSION_ID' && reauth) {
			console.log('bad sf auth');
            _this.reauthorize(app, options, module.exports.post, callback);
		} else {
			console.log('bad request:'+body);
			res.status(400);
			res.json({'error':'unable to handle sf request'});
		}	});
};

module.exports.patch = function(app, options, callback, reauth){
    reauth = typeof reauth !== 'undefined' ? reauth : true;
	var sfAccess = app.get('sfAccess');
	if(options.headers === undefined){
        options.headers = {'Authorization':'Bearer '+sfAccess.access_token};
    }else{
        options.headers.Authorization = 'Bearer '+sfAccess.access_token;
    }
	request.patch(options, function(err, res, body){
		if(!err && res.statusCode >= 200 && res.statusCode < 300){
			callback(err, res, body);
		} else if(!err && body.errorCode == 'INVALID_SESSION_ID' && reauth) {
			console.log('bad sf auth');
            _this.reauthorize(app, options, module.exports.patch, callback);
		} else {
			console.log('bad request:'+body);
			res.status(400);
			res.json({'error':'unable to handle sf request'});
		}	});
};

module.exports.get = function(app, options, callback, reauth){
    reauth = typeof reauth !== 'undefined' ? reauth : true;
	var sfAccess = app.get('sfAccess');
	if(options.headers === undefined){
        options.headers = {'Authorization':'Bearer '+sfAccess.access_token};
    }else{
        options.headers.Authorization = 'Bearer '+sfAccess.access_token;
    }
	request(options, function(err, res, body){
		if(!err && res.statusCode == 200){
			callback(err, res, body);
		} else if(!err && body.indexOf('INVALID_SESSION_ID')  > -1 && reauth) {
			console.log('bad sf auth');
            _this.reauthorize(app, options, module.exports.get, callback);
		} else {
			console.log('bad request:'+body);
			res.status(400);
			res.json({'error':'unable to handle sf request'});
		}
	});
};

module.exports.reauthorize = function(app, options, method, callback){
	var credentials = {
		'url': 'https://na16.salesforce.com/services/oauth2/token',
		'params':{
			'grant_type':'password',
			'client_id':'3MVG9fMtCkV6eLhdRfNiMN97fnUoJ1s5Ph1dtNnWAHQY5Y.vtwGne3fX7KetS6WgZq16ujb7.7C9bkAO5ov91',
			'client_secret':'9084249036812638485',
			'username':'umstedapi@gmail.com',
			'password':'MakeMoney1MpN3umsgCIAqcKWAWxuDjVrF',
			'request_uri':'https://localhost'
		}
	};
	request.post({url:credentials.url, form: credentials.params}, 
		function(err,res,body){
			if(!err && res.statusCode == 200){
				var sfAccess = JSON.parse(body);
				app.set('sfAccess', sfAccess);
                method(app, options, callback, false);
			}else{
				app.set('sfAccess', {});
				console.log('sf.reauthorize error:'+body);
			}
		});	
};
console.log('loading sf/helpers.js end');
